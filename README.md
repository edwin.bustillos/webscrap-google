WEB SCRAPING GOOGLE 
NO FINAL DA BUSCA MONTA UMA LISTA COM PALAVRAS USADAS NA PESQUISA

Para usar:
>>>python3 scrap.py busca_com_underscore (argumento)

Exemplos:

>>>python3 scrap.py receita_bolo_laranja

>>>python3 scrap.py filmes_lancamentos_2018

Requisitos:

-Python3.X.

-BeautifulSoup 4

Referências Bibliográficas de Livros e Sites:

MITCHELL - Ryan, Web Scraping com Python, São Paulo : Novatec, 2015.

SUMMERFIELD - Mark, Programação em Python, Rio de Janeiro : Alta Books, 2013.

MARTIN - Edmund, Scraping Google with Python - 2017 - http://edmundmartin.com/scraping-google-with-python/ - 19/06/2018.
