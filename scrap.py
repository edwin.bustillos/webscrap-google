import sys
import requests
from bs4 import BeautifulSoup
# Definindo Lista onde será armazenado valores
lista=[]
# Definindo Argumento para busca
param = sys.argv[1]
# Limite de Resultados 
resultados_rank = 20
# Definindo Idioma para busca Ex: 'en' para Inglês ou 'pt-br' para Português do Brasil
idioma = 'pt-br' 
# Definindo User Agente de identificação de consulta
agente = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}

# FAZENDO CONSULTA E SEPARANDO INFORMACOES 
def consulta(param, resultados_rank, idioma):
    escaped_param = param.replace(' ', '+')
    google_url = 'https://www.google.com/search?q={}&num={}&hl={}'.format(escaped_param, resultados_rank, idioma)
    response = requests.get(google_url, headers=agente)
    response.raise_for_status()
    return param, response.text
  # print(html)

# Analisando Resultado da busca
def analisar(html, keyword):
    soup = BeautifulSoup(html, 'html.parser')
    found_results = []
    rank = 1
    result_block = soup.find_all('div', attrs={'class': 'g'})
    for result in result_block:
        link = result.find('a', href=True)
        title = result.find('h3', attrs={'class': 'r'})
        description = result.find('span', attrs={'class': 'st'})
        if link and title:
            link = link['href']
            title = title.get_text()
            if description:
                description = description.get_text()
            if link != '#':
                found_results.append({'Palavra': keyword, 'Rank': rank, 'Titulo': title, 'Descricao': description, 'Endereco':link})
                rank += 1
    return found_results

keyword, html = consulta(param, resultados_rank, idioma)
lista_json = analisar(html,keyword)
#print(lista_json)
count = 0
for d in lista_json:
    count +=1
    print ('\n\n####################################################- PESQUISA',count,'-####################################################')
    for key, value in d.items():
        print (key,':', value)
        if key=='Descricao':
            #lista.insert(count-1,value)
            if value is not None:
                for i in value.split():
                    if i not in lista:
                        lista.insert(count-1,i)
print('\n\n')
print ('------------------------------------------------------- LISTA MONTADA ------------------------------------------------------\n')
print(lista,'\n')
print('Total de palavras montadas: ',len(lista))